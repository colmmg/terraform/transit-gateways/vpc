provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "main-vpc"
  }
}


resource "aws_subnet" "private-a" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.1.0/24"
  tags = {
    Name = "private-a"
  }
}

resource "aws_subnet" "private-b" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.2.0/24"
  tags = {
    Name = "private-b"
  }
}

data "terraform_remote_state" "tgw" {
  backend = "s3"
  config {
    bucket = "my-tgw-terraform-state"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

resource "aws_ec2_transit_gateway_vpc_attachment" "example" {
  subnet_ids         = ["${aws_subnet.private-a.id}", "${aws_subnet.private-b.id}"]
  transit_gateway_id = "${data.terraform_remote_state.tgw.id}"
  vpc_id             = "${aws_vpc.main.id}"
}
