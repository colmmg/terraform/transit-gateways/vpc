# transit-gateways/vpc
The transit gateways vpc Terraform code.

# Prerequisite
- Update `my-tgw-terraform-state` in the `data.terraform_remote_state.tgw` resource in `main.tf` with the S3 bucket name you created for `transit-gateways/tgw`.

# Usage
```
terraform init
terraform plan
terraform apply
```

# License and Authors
Authors: [Colm McGuigan](https://gitlab.com/colmmg)
